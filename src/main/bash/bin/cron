#!/usr/bin/bash

################################################################################
set -e
[[ "${DEBUG}" == 1 ]] && set -x
require XDG_CACHE_HOME
################################################################################

function main() {
	local -r period="${1}"

	case "${period}" in
		@daily)
			shift
			cron::daily "${@}"
			;;
		*)
			cron::repeat "${@}"
			;;
	esac
}

function cron::repeat() {
	local -r time="${1/\*/..}:${2/\*/..}"
	shift 2

	cls
	while true; do
		if [[ "$(date +"%H:%M")" =~ ${time} ]]; then
			cls
			"${@}"
		fi
		sleep $((60 - $(date +"%S")))
	done
}

function cron::daily() {
	local -r command="${1}"
	shift

	local -r stringified="$(sed -e 's/\(\.\|::\)/-/g' <<< "${command}")"
	if [[ -f "${XDG_CACHE_HOME}/${stringified}-$(date +"%Y-%m-%d")" ]]; then
		return
	fi

	"${command}" "${@}"

	remove "${XDG_CACHE_HOME}/${stringified}"-*
	touch "${XDG_CACHE_HOME}/${stringified}-$(date +"%Y-%m-%d")"
}

main "${@}"
