#!/usr/bin/bash

################################################################################
[[ -n "${__LIB_BRUSH_COMMON_LOG_SH__}" ]] && return
readonly __LIB_BRUSH_COMMON_LOG_SH__=1
################################################################################

readonly LOG="${LOG:-WARN}"

readonly -A LOG_LEVELS=(
	["JOB"]=0
	["STEP"]=1
	["ERROR"]=2
	["WARN"]=3
	["INFO"]=4
	["DEBUG"]=5
	["TRACE"]=6
)

readonly LEVEL_CODE="${LOG_LEVELS["${LOG:-"JOB"}"]:-${LOG_LEVELS["JOB"]}}"
