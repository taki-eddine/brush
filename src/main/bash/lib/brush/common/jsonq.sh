#!/usr/bin/bash

################################################################################
[[ -n "${__LIB_BRUSH_COMMON_JSONQ_SH__}" ]] && return
readonly __LIB_BRUSH_COMMON_JSONQ_SH__=1
################################################################################

function jsonq() {
	local -n filtered_results="${1}"
	local -r jpath="${2}"
	local -r json="${3}"

	local -r IFS=$'\n\r'
	local -r results=($(jq --raw-output "${jpath}" <<< "${json}"))
	for result in "${results[@]}"; do
		if [[ "${result}" ]]; then
			filtered_results+=("${result}")
		fi
	done
}

function jsonq::keys() {
	local -n keys="${1}"
	local -r jpath="${2}"
	local -r json="${3}"
	jsonq keys "${jpath} | keys_unsorted[]" "${json}"
}

function jsonq::values() {
	local -n values="${1}"
	local -r jpath="${2}"
	local -r json="${3}"
	jsonq values "${jpath} | .[]" "${json}"
}

function jsonq::type() {
	local -r jpath="${1}"
	local -r json="${2}"
	jq --raw-output "${jpath} | type" <<< "${json}"
}

function jsonq::has() {
	local -r jpath="${1}"
	local -r jhas="${2}"
	local -r json="${3}"

	local -r IFS=$'"\n\r'
	jq --raw-output "${jpath} | has(${jhas})" <<< "${json}"
}
