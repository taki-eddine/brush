#!/usr/bin/bash
# shellcheck disable=all

################################################################################
[[ -n "${__LIB_BRUSH_UTILS_COLOR_SH__}" ]] && return
readonly __LIB_BRUSH_UTILS_COLOR_SH__=1
################################################################################

readonly RESET_FG='\033[0m'

readonly BLUE_FG='\033[0;34m'
readonly BLUE_BOLD_FG='\033[1;34m'

readonly GREEN_FG='\033[0;32m'
readonly GREEN_BOLD_FG='\033[1;32m'

readonly ORANGE_FG='\033[0;33m'
readonly ORANGE_BOLD_FG='\033[1;33m'

readonly RED_FG='\033[0;31m'
readonly RED_BOLD_FG='\033[1;31m'

readonly YELLOW_FG='\033[0;33m'
readonly YELLOW_BOLD_FG='\033[1;33m'

readonly GRAY_FG='\033[0;90m'
readonly GRAY_BOLD_FG='\033[1;90m'
