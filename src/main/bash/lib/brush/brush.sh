#!/usr/bin/bash

shopt -s expand_aliases

################################################################################
[[ -n "${__LIB_BRUSH_SH__}" ]] && return
readonly __LIB_BRUSH_SH__=1
################################################################################

function use() {
	local -r function_name="${1}"
	alias "${function_name#*::}=${function_name}"
}

function using() {
	local -r namespace="${1}"
	for function_name in $(compgen -A function "${namespace}::"); do
		use "${function_name}"
	done
}
