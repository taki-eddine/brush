#!/usr/bin/bash

################################################################################
[[ -n "${__LIB_BRUSH_SYSTEM_LOGOUT_SH__}" ]] && return
readonly __LIB_BRUSH_SYSTEM_LOGOUT_SH__=1

source import "system/command/pkg.sh"
################################################################################

function system::logout() {
	system::pkg.export
}
