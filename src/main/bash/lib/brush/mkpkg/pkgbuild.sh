#!/usr/bin/bash

################################################################################
set -e
[[ -n "${__LIB_MKPKG_PKGBUILD_SH__}" ]] && return
readonly __LIB_MKPKG_PKGBUILD_SH__=1
################################################################################

function pkgbuild::clean() {
	msg.info "Removing ${GREEN_FG}src${RESET_FG} and ${GREEN_FG}pkg${RESET_FG}..."
	remove "src"/.* "src"/* "pkg"/.* "pkg"/*
}

function pkgbuild::update() {
	makepkg --nodeps --noextract --nobuild --noarchive --skipinteg --force
}

function pkgbuild::generate() {
	local md5gen
	md5gen=($(makepkg --geninteg --force 1> >(pkgbuild::accessors::md5sums.get)))

	local md5sums="("
	if [[ ${#md5gen[@]} -gt 1 ]]; then
		md5sums+="\n"
		for md5 in "${md5gen[@]}"; do
			md5sums+="\t\"${md5}\"\n"
		done
	else
		md5sums+="\"${md5gen[0]}\""
	fi
	md5sums+=")"

	pkgvar set md5sums "${md5sums}" PKGBUILD
}

function pkgbuild::extract() {
	makepkg --nodeps --holdver --nocheck --nobuild --noarchive --cleanbuild --force
}

function pkgbuild::build() {
	makepkg --nodeps --holdver --nocheck --noextract --noarchive --force
}

function pkgbuild::package() {
	makepkg --nodeps --holdver --nocheck --noextract --repackage --skipinteg --force
}

function pkgbuild::is_dirty() {
	git is-dirty
}

function pkgbuild::current() {
	git show HEAD:PKGBUILD
}

#-- accessors
function pkgbuild::accessors::md5sums.get() {
	grep --color=never --null --perl-regexp --only-matching '\w{32}'
}
###
